<?php

function decrypt($message)
{
$rtn="";
$cipher_rotate_amount=ord('M')-ord('G');

// Messages are reversed so start at the end and work towards beginning
for ($i=strlen($message)-1;$i>=0;$i--)
    {
    // Only capital letters should be decrypted, so check for letters first
    // WARNING: This does not consider the possibility of numerals being encrypted
    if ($message[$i]>'A' && $message[$i]<='Z')
        {
        $decrypted=ord($message[$i])-$cipher_rotate_amount;
        if ($decrypted<'A')
            {
            // Since this is a rotational cipher we need wrap around
            // to the end of the alphabet if we moved below 'A'
            $decrypted+=26;
            }
        $rtn.=chr($decrypted);
        }
    else
        {
        // just pass through all characters that are not capital letters
        $rtn.=$message[$i];
        }
    }
return $rtn;
}

$message="!HUP JUUM ,KJUI ZKXIKY KNZ JKZVEXIKJ KBGN AUE !YTUOZGRAZGXMTUI .XGTUY SUXL YMTOZKKXM";
echo $message;


print "\n";
print decrypt($message);
print "\n";
